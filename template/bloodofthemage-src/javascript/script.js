
/**
 * Created by curtiscampbell on 14-12-15.
 */
$(document).ready(function(){
    /**
     * All header functionality here
     */
    var Header = (function(){

        // Members
        var navContainer = $('.main-navigation');

        // Public Methods
        var toggleMobileNavigation = function()
        {
            navContainer.toggleClass('main-navigation--displayed');
        };

        // return public methods
        return {
            toggleMobileNavigation: toggleMobileNavigation
        };

    })();

    var navMobileToggle = $('.mobile-navigation');

    // Bind Swipebox
    $('a[href*=".png"], a[href*=".gif"], a[href*=".jpg"]').swipebox({
        useSVG : false
    });

    // Toggle the header
    navMobileToggle.on('click', Header.toggleMobileNavigation);
});