module.exports = function(grunt) {
	grunt.initConfig({
		/* =======================
		SASS/COMPASS
		======================= */
		compass: {
			development: {
				options: {
					sassDir: 'bloodofthemage-src/sass',
					cssDir: 'bloodofthemage-dist/css'
				}
			},
			production: {
				options: {
					sassDir: 'bloodofthemage-src/sass',
					cssDir: 'bloodofthemage-dist/css'
				}
			}
		},

		/* =======================
		SET UP THE UGLIFY TASK, will minify the JS for production builds
		======================= */
		uglify: {
			// we don't want to minfiy the code but we do want to compile it into a single file
			development: {
				options: {
					beautify: true,
					mangle: false
				},
				files: {
					'bloodofthemage-dist/javascript/bloodofthemage.min.js' : ['bloodofthemage-src/javascript/**/*.js']
				}
			},
			production: {
				options: {
					mangle: true,
					compress: {
						drop_console: true
					}
				},
				files: {
					'bloodofthemage-dist/javascript/bloodofthemage.min.js' : ['bloodofthemage-src/javascript/**/*.js']
				}
			}
		},

		/* =======================
		SET UP THE JS HINT TASK, this will provide us verbose compilation of the JavaScript providing us proper warnings and
		errors
		======================= */
		jshint: {
			development: {
				options: {
					camelcase: true,
					curly: true,
					eqeqeq: true,
					quotmark: 'single',
 					browser: true,
					globals: {
						jquery: true
					}		
				},
				files: {
					src: 'bloodofthemage-src/javascript/**/*.js'
				}
			}
		},

		/* =======================
		COPY TASK FOR IMAGES
		======================= */
		copy: {
			target: {
				files: [
					{
                        cwd: 'bloodofthemage-src/images/',
                        src: '*',
                        dest: 'bloodofthemage-dist/images/',
                        expand: true,
                        filter: 'isFile'
                    },
                    {
                        cwd: 'bloodofthemage-src/templates/',
                        src: '*',
                        dest: 'bloodofthemage-dist/',
                        expand: true,
                        filter: 'isFile'
                    }
				]
			}
		},

		/* =======================
		SET UP THE WATCH TASK, this will set up so any changes we make to any files, will automatically run the tasks
		that we provided. This is going to be the default task, but make sure we're only watching for development! not production
		======================= */
		watch: {
			stylesheets: {
				files: 'bloodofthemage-src/sass/**/*.scss',
				tasks: [ 'compass:development' ]
			},
			scripts: {
				files: 'bloodofthemage-src/javascript/**/*.js',
				tasks: [ 'uglify:development', 'jshint:development' ]
			}
		}
	});

	// Load the plugins that we want to use
	grunt.loadNpmTasks("grunt-contrib-sass");
	grunt.loadNpmTasks("grunt-contrib-compass");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-copy");

	// register default task for watch
	grunt.registerTask("default", ["development"]);

	// register development site task
	grunt.registerTask("development", ["compass:development", "uglify:development", "jshint:development", "copy"]);

	// register the production site task
	grunt.registerTask("production", ["compass:production", "uglify:production", "copy"])
}