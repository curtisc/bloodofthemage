<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bloodofthemage');
//define('DB_NAME', 'curtisc_bloodofthemage');

/** MySQL database username */
define('DB_USER', 'root');
//define('DB_USER', 'curtisc_admin');

/** MySQL database password */
define('DB_PASSWORD', '');
//define('DB_PASSWORD', 'Admin1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q4H<v]|_PlZ2:muYD-oc<4;^^N`% A8Qp9Mu}0R]MDs8vOOu>YFuTp6Vx_XYbDU|');
define('SECURE_AUTH_KEY',  '8gsWPlU._${D1:? t-Z5yV}EJza/K|`EJ!m1[(GXrNJ5>~-|}*s?6Ux^a>G%{jz$');
define('LOGGED_IN_KEY',    ' drGo_q7eYc]x?tK|fJB){MA:9??3<;bG],!%N>1j*!>NBv<(`Xnm[sJiY2t5t<q');
define('NONCE_KEY',        'x{xYkCn5IvA1f-b6kzQf{ZY&5&5aweP2{Ya*+e>I^TkVgq4@Vzb Ns+{M]s-|YBN');
define('AUTH_SALT',        '{d1/D&{<S9B>p#PVSEkLTo{-o5Zv`(1V2vx`Fbe3yRYL^B;GM4[#i12rAFw- ?`#');
define('SECURE_AUTH_SALT', 'w?m$awv6gbV,F4||&|;>g52P-t@a8jq->/*Ty*+)/,3h>LtLnZYY=)<?+z0M${05');
define('LOGGED_IN_SALT',   '9K/pTTY*TU;LpXk~}Yu|XiU@m@s~;(<L5ach(++^h-d,VdN_Z.|`]3RVEHXt|6-!');
define('NONCE_SALT',       '@R%[KzHyG7%?-W^-Vz=hfJx_;?)0uyn*r3f!`|ZVkbWU/Q9p9>@y:CB9eu|]l/iL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
