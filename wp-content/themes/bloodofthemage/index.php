<?php get_header(); ?>
<div class="content-container grid">
    <section class="content col-10-6">
        <div class="blog-container">
            <?php if ( have_posts() ) : ?>

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'content', get_post_format() );

                endwhile;

                the_posts_pagination( array(
                    'prev_text'          => __( 'Previous page', 'bloodofthemage' ),
                    'next_text'          => __( 'Next page', 'bloodofthemage' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'bloodofthemage' ) . ' </span>',
                ) );

            else :
                get_template_part( 'content', 'none' );

            endif;
            ?>
        </div>
    </section>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>