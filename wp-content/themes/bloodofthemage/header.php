<!DOCTYPE html>
<html>
<head <?php language_attributes(); ?> >
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/javascript/libs/swipebox/js/jquery.swipebox.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/javascript/bloodofthemage.min.js"></script>

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo( 'template_url' ); ?>/javascript/libs/swipebox/css/swipebox.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
</head>
<body>

<div class="header-container">
    <header class="header grid">
        <div class="site-title">
            <h1><a href="<?php bloginfo( 'url' ); ?>" title="Blood of the Mage"><?php bloginfo('name'); ?></a></h1>
            <h2><?php bloginfo('description'); ?><</h2>
        </div>
        <div class="mobile-navigation"></div>
        <nav class="main-navigation">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'primary',
            ) );
            ?>
        </nav>
    </header>
</div>