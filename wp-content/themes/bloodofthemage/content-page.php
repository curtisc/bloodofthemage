<header>
    <?php the_title( '<h1>', '</h1>' ); ?>
</header>

<?php the_content(); ?>
<?php edit_post_link( __( 'Edit', 'bloodofthemage' ), '<footer>', '</footer>' ); ?>

