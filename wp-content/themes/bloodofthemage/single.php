<?php get_header(); ?>

<div class="content-container grid">
    <section class="content col-10-6">
        <div class="blog-container">

        <?php
        while ( have_posts() ) : the_post(); ?>

            <div class="blog-post">
            <?php the_post_thumbnail(); ?>
                <header>
                    <a href="javascript:javascript:history.go(-1)" class="button blog-post--back">Back</a>
                    <?php the_title( sprintf( '<h1><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
                    <?php
                    if(get_the_tag_list()) {
                        echo '<p>';
                        echo get_the_tag_list('<strong>Tags: </strong>', ', ');
                        echo '</p>';
                    }
                    ?>
                    <p>

                    </p>
                </header>
                <div class="blog-post--content">
                    <?php the_content(); ?>
                </div>

                <?php if ( 'post' == get_post_type() ) : ?>
                <div class="blog-post--vitals">
                   <p><?php edit_post_link(); ?></p>
                </div>
                <?php endif; ?>
            </div>

            <?php

            wp_reset_query();
            comments_template();

        endwhile;
        ?>

        </div>
    </section>
    <?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
