<?php

if ( ! function_exists( 'bloodofthemage_setup' ) ) :

function bloodofthemage_setup() {

    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'title-tag' );


    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    register_nav_menus( array(
    'primary' => __( 'Primary Menu',      'bloodofthemage' ),
    ) );

    add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    add_theme_support( 'post-formats', array(
    'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );

    add_theme_support( 'custom-background', apply_filters( 'bloodofthemage_custom_background_args', array(
    'default-color'      => '#000',
    'default-attachment' => 'fixed',
    ) ) );

}
endif;
add_action( 'after_setup_theme', 'bloodofthemage_setup' );

function bloodofthemage_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Widget Area', 'bloodofthemage' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'bloodofthemage' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'bloodofthemage_widgets_init' );

// get the template tags
require get_template_directory() . '/inc/template-tags.php';

?>