<div class="blog-post">
    <?php the_post_thumbnail(); ?>
    <header>
        <?php the_title( sprintf( '<h1><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
    </header>
    <div class="blog-post--content">
        <?php the_excerpt(); ?>
    </div>

    <?php if ( 'post' == get_post_type() ) : ?>
        <div class="blog-post--vitals">
            <p><a href="<?php the_permalink(); ?>" title="Read More" class="button">Read More</a></p>
            <p><?php edit_post_link( __( 'Edit', 'bloodofthemage' ), '' ); ?></p>
        </div>
    <?php else : ?>
        <?php edit_post_link( __( 'Edit', 'bloodofthemage' ), '' ); ?>
    <?php endif; ?>
</div>