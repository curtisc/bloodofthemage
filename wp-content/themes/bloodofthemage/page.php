<?php get_header(); ?>
    <div class="content-container grid">
        <section class="content col-10-6">
            <?php
            while ( have_posts() ) : the_post();
                get_template_part( 'content', 'page' );
            endwhile;
            ?>
        </section>
        <?php get_sidebar(); ?>
    </div>
<?php get_footer(); ?>